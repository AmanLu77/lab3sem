﻿//хеш-таблица с наложением(open addressing)

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

const int N = 10; //размер таблицы по умолчанию
struct Element
{
	std::string key;
	int value;
	//конструктор по умолчанию
	Element(std::string key = "", int value = 0) : key(key), value(value) {}
};

class HashMap
{
	int size = N; //текущий размер таблицы
	std::vector <Element> table;

	// хеш функция для присваивания индекса 
	int hash(std::string s)  //на вход элемент
	{
		int h = 0, k = 1;
		for (char c : s) // h = с0 + c1*k^1 + c2*k^2 + ... , k=3
		{
			h += c * k;
			k *= 3;
		}
		return h % N;
	}

public:
	//конструктор по умолчанию
	HashMap()
	{
		for (int i = 0; i < N; i++)
			table.push_back(Element("", 0)); //заполнение пустыми элементами
	}

	//запись таблицы
	void Add(std::string s)
	{
		int h = hash(s);
		//перебираем места в таблице пока не найдем свободное место или нужный ключ или закончится таблица
		while (table[h].key != "" && table[h].key != s && h < (size - 1))
			h++;
		if (table[h].key == "") //нашлось свободное место - записываем сюда элемент
			table[h] = Element(s, 1);
		else if (table[h].key == s) //нашелся нужный ключ - увеличиваем значение ключа							  ///////////////////////
			table[h].value++;
		else //не нашлось ни места ни ключа и закончилась таблица
		{
			size++; //увеличиваем таблицу и записываем на новое место новый элемент
			table.push_back(Element(s, 1));
		}
	}

	//вывод таблицы
	std::string Write()
	{
		std::string tmp;
		for (int i = 0; i < table.size(); i++) //пробегаемся по таблице и если нашлось ненулевое значение - выводим это значение и его ключ
			if (table[i].value)
				tmp += table[i].key + ' ' + std::to_string(table[i].value) + '\n';
		return tmp;
	}

};

int main()
{
	std::ifstream in("input13.txt");
	std::ofstream out("output13.txt");

	HashMap table;
	std::string str;
	while (in >> str) table.Add(str);
	out << table.Write();

	in.close();
	out.close();

	return 0;
}