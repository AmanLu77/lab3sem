//Внешняя многофазная ▲▼▲▼▲
#include <fstream>
#include<iostream>


int main()
{
    //три входа и три выхода
    std::ifstream in, in1, in2; //основной и два вспом.
    std::ofstream out, out1, out2; //основной и два вспом.

    int x, y; ////
    int size = 0; //количество чисел на вход
    int counter_A, counter_B, counter; ////
    bool flag = true;
    bool A_contains, B_contains;

    //открытие файлов
    in.open("input12.txt");
    out.open("output12.txt");

    //считаем кол-во чисел и добавляем их в файл вывода
    while (in >> x)
    {
        out << x << " ";
        size++;
    }
    in.close();
    out.close();

    //вспомогательная сортировка(разделение на подфайлы)
    //рассматриваем элементы с промежутком 1,2,4,8 и тд
    for (int interval = 1; interval < size; interval *= 2)
    {
        in.open("output12.txt");
        out1.open("A.txt");
        out2.open("B.txt");
        counter = 0;

        //записываем числа в А и в В:
        //первая пара чисел на расстоянии 1 в А; вторая пара чисел на расстоянии 1 в B 
        //первая пара чисел на расстоянии 2 в А; вторая пара чисел на расстоянии 2 в B и тд
        while (in >> x)
        {
            counter++;
            //добавляем число в А
            if (flag) out1 << x << " ";
            //добавляем число в В
            else out2 << x << " ";
            //когда распределили все числа при данном интервале по файлам А и В 
            if (counter == interval) {
                counter = 0;
                flag = !flag;
            }
        }
        //закрываем файлы
        in.close();
        out1.close();
        out2.close();
        //очищаем файл
        remove("output12.txt");
        //открываем
        in1.open("A.txt");
        in2.open("B.txt");
        out.open("output12.txt");

        //проверяем остались ли числа в том же файле, в котором и были
        if (in1 >> x) A_contains = true;
        else A_contains = false;

        if (in2 >> y) B_contains = true;
        else B_contains = false;

        //основная сортировка
        for (int i = 0; i < size; i += 2 * interval)
        {
            counter_A = 0; counter_B = 0; //кол-во чисел в подфайлах
            //сравниваем по 1 числу из файла А и В, потом по 2 пары чисел, потом по 4 пары,тк partSize *= 2
            while (counter_A < interval && A_contains && counter_B < interval && B_contains)
            {
                //если (x < y) добавляем в А и считаем сколько там чисел
                if (x < y) {
                    out << x << " ";
                    if (in1 >> x) A_contains = true;
                    else A_contains = false;
                    counter_A++;
                }
                //елси (x > y) добавляем в В и считаем сколько там чисел 
                else {
                    out << y << " ";
                    if (in2 >> y) B_contains = true;
                    else B_contains = false;
                    counter_B++;
                }
            }
            //если остались числа в А
            while (counter_A < interval && A_contains)
            {
                out << x << " ";
                if (in1 >> x) A_contains = true;
                else A_contains = false;
                counter_A++;
            }
            //если остались числа в В
            while (counter_B < interval && B_contains)
            {
                out << y << " ";
                if (in2 >> y) B_contains = true;
                else B_contains = false;
                counter_B++;
            }
        }
        //закрываем и очищаем файлы
        in1.close();
        in2.close();
        out.close();
        remove("A.txt");
        remove("B.txt");
    }
    return 0;
}