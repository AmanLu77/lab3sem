﻿//хеш таблица со списками(chained addressing)

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

const int N = 10; //размер таблицы по умолчанию
struct Element
{
	std::string key;
	int value;
	//конструктор по умолчанию
	Element(std::string key = "", int value = 0) : key(key), value(value) {}
};

class HashMap
{
	//int N = 10; //размер таблицы по умолчанию
	std::vector <Element> table[N];

	// хеш функция для присваивания индекса
	int hash(std::string s)
	{
		int h = 0, k = 1;
		for (char c : s) // h = c0 + c1*k^1 + с2*k^2 + ... , k=3
		{
			h += c * k;
			k *= 3;
		}
		return h % N;
	}

public:
	//конструктор по умолчанию
	HashMap()
	{
		for (int i = 0; i < N; i++)
			table[i].push_back(Element("", 0)); //пустые элементы
	}

	//запись таблицы
	void Add(std::string s)
	{
		bool flag = false; //записан ли элемент?
		int h = hash(s);
		if (table[h][0].key == "") //проверка свободного первого места с соотв. индексом
		{
			table[h][0] = Element(s, 1); //если да, сразу записываем элемент
			flag = true; //элемент записан? да
		}
		else //если место занято
			for (int i = 0; i < table[h].size(); i++) //проходимся по таблице в поисках элемента с таким же индексом
				if (table[h][i].key == s) //если нашли
				{
					table[h][i].value++; //увеличиваем значение при этом ключе
					flag = true; //элемент записан? да
				}
		if (!flag) //элемент записан? нет
			table[h].push_back(Element(s, 1)); //записываем его в конец
	}

	//вывод таблицы
	std::string Write()
	{
		std::string s;
		for (int i = 0; i < N; i++)
			if (table[i][0].key != "") 
			{	//если нашшлось непустое место, выводим хеш-индекс
				s += std::to_string(hash(table[i][0].key)) + '	';
				for (int j = 0; j < table[i].size(); j++) //для каждого непустого места выводим ключи и значения элементов содержащихся там
					s += table[i][j].key + ' ' + std::to_string(table[i][j].value) + ';' +	' ';
				s += '\n';
			}
		return s;
	}
};

int main()
{
	std::ifstream in("input14.txt");
	std::ofstream out("output14.txt");

	HashMap table;
	std::string str;
	while (in >> str) table.Add(str);
	out << table.Write();

	in.close();
	out.close();

	return 0;
}