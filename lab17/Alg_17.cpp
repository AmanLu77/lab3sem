﻿// Операции над БДП : поиск, добавление, удаление

#include <iostream>
#include <vector>
#include <string>

//обработка входящих данных типа 8(3(1,6(4,7)),10(,14(13,)))	
struct BinaryNode
{
	int value;
	BinaryNode* left = nullptr;
	BinaryNode* right = nullptr;

	BinaryNode(std::string& s)                               
	{
		value = getValue(s); 
		s.erase(0, amountDigits(value)); //удаляем из строки символы с 0-го по последний символ числа value
		if (s[0] == '(') s = delbracket(s); else return;   //если первый символ "(" то удаляем внешние скобки
		if (isDigit(s[0])) left = new BinaryNode(s);  //если первый символ число создаем узел с этим числом слева
		if (s[0] == ',' && isDigit(s[1])) right = new BinaryNode(s.erase(0, 1));
	}

	//вставка нового элемента
	void Insert(int x)
	{
		std::string s = std::to_string(x); //преобразовываем число в строку
		if (x > value && right) right->Insert(x); //если Х больше и справа есть потомок рекурсивно вставляем Х в правое поддерево
		if (x > value && !right) right = new BinaryNode(s); //если потомка нет создаем нового потомка для Х
		if (x < value && left) left->Insert(x); //если Х больше и справа есть потомок рекурсивно вставляем Х в правое поддерево
		if (x < value && !left) left = new BinaryNode(s); //если потомка нет создаем нового потомка для Х
	}

	//удаление
	/*
	ищем элемент Х:
	если Х > значения текущего узла и есть правый потомок
		если значение потомка совпало с Х, то удаляем потомка, 
		если НЕ совпало значение, то вызываем рекурсивно delete для правого поддерева(т.е. ищем Х в правом поддереве)
	если Х < текущего узла и есть левый потомок
		если значение потомка совпало с Х, то удаляем потомка, 
		если НЕ совпало значение, то вызываем рекурсивно delete для левого поддерева(т.е. ищем Х в правом поддереве)
	*/
	void Delete(int x)
	{
		if (x > value && right && right->value == x) right = nullptr;
		if (x > value && right && right->value != x) right->Delete(x);
		if (x < value && left && left->value == x) left = nullptr;
		if (x < value && left && left->value != x) left->Delete(x);
	}
	
	//поиск
	bool Search(int x)
	{
		return find(x);
	}

	BinaryNode* find(int x)
	{
		if (x == value) return this; //если значение совпало с Х возвращаем указатель на элемент с таким значением
		if (x > value && right) return right->find(x); //если Х > значения текущего узла и есть правый потомок, то ищем в правом поддереве
		if (x < value && left) return left->find(x); //если Х < значения текущего узла и есть левый потомок, то ищем в левом поддереве
		return nullptr; //иначе элемента нет
	}

	//записываем значения узлов в вектор v
	void getElem(std::vector<int>& v)
	{
		v.push_back(value);
		if (left) left->getElem(v);
		if (right) right->getElem(v);
	}

private:
	bool isDigit(char c) { return c >= '0' && c <= '9'; } //проверка символа на число
	int amountDigits(int n) { return n ? floor(log10(n) + 1) : 1; } //узнаем длину числа n

	//получаем значение числового элемента
	int getValue(std::string& s)
	{
		int n = s.size(), i = 1;
		while (i < n) if (!isDigit(s[i++])) break; //как только встретилась не цифра выходим
		return std::stoi(s.substr(0, i)); //преобразуем подстроку в целое число "245" в 245
	}

	//удаляем внешние скобки в строке
	std::string delbracket(std::string s)
	{
		for (int i = 1, k = 1; i < s.size(); i++, k += (s[i] == '(') - (s[i] == ')'))
			if (!k) return s.erase(0, 1).erase(i - 1, 1); //как только d==0 удаляем символы 0-й и (i-1)-й
		return s;
	}
};

class BinaryTree
{
	BinaryNode* root = nullptr;

	//печать дерева
	void Print(const std::string& s, const BinaryNode* n, bool k)
	{
		if (!n) return; //если элемента нет заканчиваем печать
		std::cout << s << (k ? "|__" : "|__") << n->value << "\n";
		Print(s + (k ? "|   " : "    "), n->left, 1);
		Print(s + (k ? "|   " : "    "), n->right, 0);
	}

public:
	BinaryTree(std::string s = "")
	{
		if (!s.empty()) root = new BinaryNode(s);
	}

	//печать дерева
	void Print() { if (root) Print("", root, 0); else std::cout << "|--\n"; };

	//
	void Insert(int x)
	{
		std::string s = std::to_string(x);
		if (root) root->Insert(x);
		else root = new BinaryNode(s);
	}

	void Delete(int x)
	{
		if (!root) return; //если нет корня выходим

		BinaryNode* n;
		if (root->value == x) n = root; //если х == значению корня то n=корень
		else n = root->find(x); //иначе ищем в дереве

		std::vector<int> branch;
		if (n->left) n->left->getElem(branch); //если есть левое поддерево то записывем значения его элементов в вектор
		if (n->right) n->right->getElem(branch); //если есть правое поддерево то записывем значения его элементов в вектор

		if (root->value == x) root = nullptr; //удаляем корень
		else root->Delete(x); //удаляем элемент в дереве
		for (int e : branch) Insert(e); //вставляем все элементы из branch в измененное поддерево
	}

	bool Search(int x) { return root ? root->Search(x) : false; }
};

int main()
{
	BinaryTree tree;
	std::string command;
	int x;
	tree.Print();
	std::cout << "Enter a number to choose command:\n 1 - Add new element;\n 2 - Delete element;\n 3 - Search element\n 4 - End.\nThen enter number.\n";
	while (1)
	{
		std::cin >> command >> x;
		if (command == "1") tree.Insert(x), tree.Print();
		if (command == "2") tree.Delete(x), tree.Print();
		if (command == "3") std::cout << x << (tree.Search(x) ? " Found!\n" : " Not found!\n");
		if (command == "4") { std::cout << "Finished successfully!"; break; }
	}
}