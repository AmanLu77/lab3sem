﻿// рекурсивные обходы (прямой, центральный, концевой)

#include <iostream>			
#include <string>

//обработка входящих данных типа 8(3(1,6(4,7)),10(,14(13,)))	
struct BinaryNode
{
	int value;
	BinaryNode* left = nullptr;
	BinaryNode* right = nullptr;

	BinaryNode(std::string& s) 
	{
		value = getValue(s);
		s.erase(0, amountDigits(value)); //удаляем из строки символы с 0-го по последний символ числа value
		if (s[0] == '(') s = delbracket(s); else return;  //если первый символ "(" то удаляем внешние скобки
		if (isDigit(s[0])) left = new BinaryNode(s);  //если первый символ число создаем узел с этим числом слева
		if (s[0] == ',' && isDigit(s[1])) right = new BinaryNode(s.erase(0, 1)); // если первый символ "," и следующий число то создаем новый узел с этим числом справа
	}

private:
	bool isDigit(char c) { return c >= '0' && c <= '9'; } //проверка символа на число
	//floor выполняет округление значения (log10(n) + 1) и возвращает наибольшее целое значение, которое не больше, чем (log10(n) + 1)
	int amountDigits(int n) { return n ? floor(log10(n) + 1) : 1; }//узнаем длину числа n

	//получаем значение числового элемента
	int getValue(std::string& s)
	{
		int n = s.size();
		int i = 1;
		while (i < n) if (!isDigit(s[i++])) break; //как только встретилась не цифра выходим
		return std::stoi(s.substr(0, i)); //преобразуем подстроку в целое число "245" в 245
	}

	//удаляем внешние скобки в строке
	std::string delbracket(std::string s)
	{
		for (int i = 1, d = 1; i < s.size(); i++, d += (s[i] == '(') - (s[i] == ')'))
			if (!d) return s.erase(0, 1).erase(i - 1, 1); //как только d==0 удаляем символы 0-й и (i-1)-й
		return s;
	}
};

class BinaryTree
{
	BinaryNode* root = nullptr; //корень

	//печать дерева
	void Print(const std::string& s, const BinaryNode* n, bool k) {
		if (n == nullptr) return;  //если элемента нет заканчиваем печать
		std::cout << s << (k ? "|__" : "|__") << n->value << "\n";
		Print(s + (k ? "|   " : "    "), n->left, 1);
		Print(s + (k ? "|   " : "    "), n->right, 0);
	}

public:
	BinaryTree(std::string s) : root(new BinaryNode(s)) {}
	
	//печать дерева
	void print() { Print("", root, 0); };

	//прямой обход
	/*
	1) Проверяем, не является ли текущий узел пустым или null.
	2) Показываем поле данных корня (или текущего узла).
	3) Обходим левое поддерево рекурсивно, вызвав функцию прямого обхода.
	4) Обходим правое поддерево рекурсивно, вызвав функцию прямого обхода.
	*/
	void Direct(BinaryNode* n)    
	{
		if (!n) return;
		std::cout << n->value << " ";
		Direct(n->left);
		Direct(n->right);
	}
	void Direct() { Direct(root); std::cout << "\n"; }

	//центральный обход (симметричный от центра)
	/*
	1) Проверяем, не является ли текущий узел пустым или null.
	2) Обходим левое поддерево рекурсивно, вызвав функцию центрированного обхода.
	3) Показываем поле данных корня (или текущего узла).
	4) Обходим правое поддерево рекурсивно, вызвав функцию центрированного обхода
	*/
	void Central(BinaryNode* n)       
	{
		if (!n) return;
		Central(n->left);
		std::cout << n->value << " ";
		Central(n->right);
	}
	void Central() { Central(root); std::cout << "\n"; }

	//концевой обход
	/*
	1) Проверяем, не является ли текущий узел пустым
	2) Обходим левое поддерево рекурсивно, вызвав функцию обратного обхода
	3) Обходим правое поддерево рекурсивно, вызвав функцию обратного обхода
	4) Выводим поле значение корня (или текущего узла).
	*/
	void Inversed(BinaryNode* n)    
	{
		if (!n) return;
		Inversed(n->left);
		Inversed(n->right);
		std::cout << n->value << " ";
	}
	void Inversed() { Inversed(root); std::cout << "\n"; }
};

int main()
{
	BinaryTree tree("8(3(1,6(4,7)),10(,14(13,)))");				//			 8			
	tree.print(); //рекурсивный									//		3		  10	
	tree.Direct(); //прямой 									//	 1	  6		     14	
	tree.Central(); //центральный								//		 4  7		   13
	tree.Inversed(); //концевой
}