﻿#include <iostream>
using namespace std;
int OTKR = 0;

//считываем число
float number()
{
    int res = 0;
    for (;;)
    {
        int i;
        char c = cin.get();
        if (c >= '0' && c <= '9')
        {
            i = cin.get(); //проверка на ошибочный символ после цифры
            if (i == '(')
            {
                cout << "После цифры открывающая скобка";
                exit(-1);
            }
            else
            {
                cin.putback(i);
            }
            res = res * 10 + c - '0';
        }
        else if(c == ')')
        {
            OTKR--;
            if (OTKR < 0) {
                std::cout << "Не хватает открывающей скобки";
                exit(-1);
            }

            i = cin.get(); //проверка на ошибочный символ после цифры
            if (!(i == '+' || i == '-' || i == '/' || i == '*' || i == ')' || i == '\n'))
            {
                cout << "После закрывающей скобки некорректный символ";
                exit(-1);
            }
            else 
            {
                cin.putback(i);
            }
        }       
        else 
        {
            cin.putback(c);
            return res;
        }
    }
}

float expr();

//считаем выражения в скобках 
float skobki()
{
    int i;
    char c = cin.get();
    if (c == '(')
    {
        OTKR++;

        i = cin.get();
        if (!(i >= '0' && i <= '9' || i == '('))
        {
            std::cout << "Некорректный символ после откр скобки";
            exit(-1);
        }
        else cin.putback(i);

        float x = expr();
        c = cin.get();
        if (c != ')')
            cin.putback(c);
        return x;
    }
    else
    {
        cin.putback(c);
        return number();
    }
}

//выполняем * и / посчитав выражения в скобках
float factor()
{
    float x = skobki();
    for (;;)
    {
        int k, i;
        char c = cin.get();
        switch (c)
        {
        case '*':
            i = cin.get(); //проверка на корректность следующего символа
            if (!(i >= '0' && i <= '9' || i == '('))
            {
                std::cout << "После * нет операнда";
                exit(-1);
            }
            else cin.putback(i);

            x *= skobki();
            break;

        case '/':
            i = cin.get(); //проверка на корректность следующего символа
            if (!(i >= '0' && i <= '9' || i == '('))
            {
                std::cout << "После / нет операнда";
                exit(-1);
            }
            else cin.putback(i);

            //проверка на деление на 0
            k = skobki();
            if (k == 0) {
                std::cout << "Деление на ноль";
                exit(-1);
            }
            else
            {
                x /= k;
                break;
            }
        default:
            cin.putback(c);
            return x;
        }
    }
}

//выполняем + и -  посчитав выражения в скобках и * и /
float expr()
{
    float x = factor();
    for (;;)
    {
        int i;
        char c = cin.get();
        switch (c)
        {
        case '+':
            i = cin.get();
            if (!(i >= '0' && i <= '9' || i == '('))
            {
                std::cout << "После + некорректный символ";
                exit(-1);
            }
            else
            {
                cin.putback(i);
                x += factor();
                break;
            }
        case '-':
            i = cin.get();
            if (!(i >= '0' && i <= '9' || i == '('))
            {
                std::cout << "После - нет операнда";
                exit(-1);
            }
            else
            {
                cin.putback(i);
                x -= factor();
                break;
            }
        default:
            cin.putback(c);
            return x;
        }
    }
}

int main()
{
    setlocale(LC_ALL, "rus");

    cout << "Введите выражение: ";
    float res = expr();

    if (OTKR == 0) {
        cout << "Результат: " << res << endl;
        return 0;
    }
    else {
        std::cout << "Неправильно расставлены скобки";
        return -1;
    }

    //   (10+15)-4*5/2            //правильная
    //   (10+15))-4*5/2           //лишняя )
    //   ((10+15)-4*5/2           //лишняя (
    //   (10+15)-4*5/0            //деление на 0
    //   (10+15)-4*5/(100-100)    //деление на нулевую скобку
}