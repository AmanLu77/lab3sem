﻿/*
Лаб 7) Шелла •••
Лаб 8) Поразрядная •••
Лаб 9) Пирамидальная (heap sort) •••
*/
#include <iostream>
#include <vector>
#include <math.h>
#include <ctime>

void FillArr(std::vector<int>& arr, int M)
{
	// Заполнение массива случайными числами
	for (int i = 0; i < M; i++)
	{
		arr.push_back(rand() % 1000 + 1); // числа в диапазоне от 0 до 100 вкл-но
	}
}

void PrintArray(std::vector<int> arr)
{
	int n = arr.size();
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";
	std::cout << '\n';
}

//СОРТИРОВКА ШЕЛЛА
/*
Сложность:	Best: O(nlog(n)),
			Average: O(nlog^2(n)) **Зависит от выбора h,	
			Worst: O(n^2)
Как работает: 
Сначала сравниваются и сортируются между собой ключи, 
отстоящие один от другого на некотором расстоянии h. 
Полученная последовательность элементов в списке называется отсортированной по h. 
После этого процедура повторяется для некоторых меньших значений h (h/2).
Завершается сортировка Шелла упорядочиванием элементов при h = 1 
(то есть, обычной сортировкой вставками). 
Например в массиве длиной 10 сравнения происходят так: (h=n/2)
h=5: 0и5, 1и6, 2и7, 3и8, 4и9;
h=2: 0и2, 1и3, 2и4, 3и5, 4и6, 5и7, 6и8, 7и9;
h=1: 0и1, 1и2, 2и3, 3и4, 4и5, 5и6, 6и7, 7и8, 8и9;
*/
void ShellSort(std::vector<int>& arr)
{
	//узнаем размер массива
	int n = arr.size();
	//определяем первое расстояние для сортировки
	int h = n / 2;
	while (h > 0)
	{
		for (int i = h; i < n; i++)
		{
			int x = arr[i]; //элемент[i] массива
			int j = i - h;	//индекс элемента слева, отстоящего на h от элемента[i] 
			while (j >= 0 && x < arr[j])
			{
				//пока есть числа на расстоянии h до элемента[j] и элемент[i] < элемента[j]
				arr[j + h] = arr[j]; //больший элемент записываем на место меньшего
				j = j - h; //сдвигаем индекс на расстояние h влево
			}
			arr[j + h] = x; //записываем элемент[i] на место [j+h]
		}
		//уменьшаем расстояние
		h = h / 2;
	}
}

//ПИРАМИДАЛЬНАЯ СОРТИРОВКА
/*
Сложность: O(nlog(n)) в любых случаях
Как работает: сначала образуется двоичная куча для исходного массива
(В данном алгоритме вершиной кучи является максимальный элемент массива)
Затем вершина меняется местами с последним элементом и задача сводится к сортировке подмассивов длины size-1.
Для каждого подмассива образуется двоичная куча и процедура повторяется до того момента
пока не отсортируется массив длины 1.
*/
//Для создания двоичной кучи
void MaxHeap(std::vector<int>& arr, int size, int i)
{
    int high = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < size && arr[left] > arr[high])
        high = left;

    if (right < size && arr[right] > arr[high])
        high = right;

    if (high != i)
    {
        std::swap(arr[i], arr[high]);
		//std::cout << "MaxHeap\n"; PrintArray(arr); //TEST
        MaxHeap (arr, size, high);
    }
}

void HeapSort(std::vector<int>& arr, int size)
{
    for (int i = size / 2 - 1; i >= 0; i--)
        MaxHeap(arr, size, i);

    for (int i = size - 1; i >= 0; i--)
    {
        std::swap(arr[0], arr[i]);
		//std::cout << "for\n"; PrintArray(arr); //TEST
        MaxHeap(arr, i, 0); 
    }
}

//ПОРАЗРЯДНАЯ СОРТИРОВКА LSD
/*
Как работает:
создается вспомогательный массив bucket такого же размера как и исходный массив arr. 
Определяется максимальный разряд для определения количества циклов сортировки.
В первом цикле сортировка производится по единицам, в следующем - по десяткам и т.д.
Далее заходим в цикл, который выполняется до тех пор, пока максимальный разряд превышает только что отсортированный.
В цикле заполняются такие массивы: 
digitCount - количество одинаковых значений разрядов
(далее они последовательно суммируются для того чтобы элементы исходного массива вставлялись на свои места);
bucket - вспом.массив для отсортированных значений исходного массива которые далее записываются в arr.
Как только все разряды отсортированы выходим из цикла и сортировка заканчивается.
*/
void RadixSortLSD(std::vector<int>& arr, int arraySize)
{
	std::vector<int> bucket;
	//заполнение вспом.массива нулями, чтобы можно было вставлять значения на определенные позиции
	for (int i = 0; i < arraySize; i++) 
	{
		bucket.push_back(0);
	}
	int maxVal = 0; //максимальный разряд = количество циклов сортировки
	int digitPosition = 1; //разряд по котоорому сортируем

	//ищем максимальный разряд
	for (int i = 0; i < arraySize; i++)
	{
		if (arr[i] > maxVal)
			maxVal = arr[i];
	}

	int pass = 1;  // для контроля циклов сортировки

	//пока максимальный разряд превышает только что отсортированный
	while (maxVal / digitPosition > 0)
	{
		//обнуление счетчика значений разрядов
		int digitCount[10] = { 0 };

		//посчитать количество одинаковых значений разрядов
		for (int i = 0; i < arraySize; i++)
			digitCount[arr[i] / digitPosition % 10]++;

		//Последовательное суммирование одинаковых разрядов
		//из {3,0,0,1,0,1,2,0,1} в {3,3,3,4,4,5,7,7,8}
		for (int i = 1; i < 10; i++)
			digitCount[i] += digitCount[i - 1];

		//записать в вспом.массив
		for (int i = arraySize - 1; i >= 0; i--)
			bucket[--digitCount[arr[i] / digitPosition % 10]] = arr[i];

		//записать в исходный массив
		for (int i = 0; i < arraySize; i++)
			arr[i] = bucket[i];

		//вывод массива отсортированного по разряду digitPosition
		std::cout << "pass #" << pass++ << ": ";
		PrintArray(arr);

		//cмещаем разряд влево(увеличиваем в 10 р)
		digitPosition *= 10;
	}
}

int main()
{
	srand(time(0));

	std::vector<int> arr1;
	std::vector<int> arr2;
	std::vector<int> arr3;
	FillArr(arr1, 10); //заполнение массива случайными числами
	FillArr(arr2, 10);
	FillArr(arr3, 10);
	int n2 = arr2.size(); //размер массива
	int n3 = arr3.size();

	std::cout << "\nShellSort\n" << "Initial Array:\n"; PrintArray(arr1);
	ShellSort(arr1);
	std::cout << "Sorted Array:\n"; PrintArray(arr1);

	std::cout << "\nHeapSort\n" << "Initial Array:\n"; PrintArray(arr2);
	HeapSort(arr2, n2);
	std::cout << "Sorted Array:\n"; PrintArray(arr2);

	std::cout << "\nRadixSortLSD\n" << "Initial Array: \n"; PrintArray(arr3);
	RadixSortLSD(arr3, n3);
	std::cout << "Sorted Array:\n"; PrintArray(arr3);


	return 0;
}