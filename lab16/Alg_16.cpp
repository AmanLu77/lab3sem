﻿/* 
“Не рекурсивный прямой обход”(реализуется с помощью стека).
В качестве выходных данных формируется строка обхода.
Например: Бинарное дерево поиска
*/
																	   
#include <iostream>															   
#include <vector>															   
#include <string>																   
																		
//обработка входящих данных 8(3(1,6(4,7)),10(,14(13,)))					
struct BinaryNode														
{                                       								
	int value;															
	BinaryNode* left = nullptr;	
	BinaryNode* right = nullptr;

	BinaryNode(std::string& s)
	{
		value = getValue(s);
		s.erase(0, amountDigits(value)); //удаляем из строки символы с 0-го по последний символ числа value
		if (s[0] == '(') s = delbracket(s); else return; //если первый символ "(" то удаляем внешние скобки
		if (isDigit(s[0])) left = new BinaryNode(s); //если перый символ число создаем узел с этим числом слева
		if (s[0] == ',' && isDigit(s[1])) right = new BinaryNode(s.erase(0, 1)); // если первый символ "," и следующий число то создаем новый узел с этим числом справа
	}

private:
	bool isDigit(char c) { return c >= '0' && c <= '9'; } //проверка символа на число
	
	int amountDigits(int n) { return n ? floor(log10(n) + 1) : 1; } //узнаем длину числа n

	//получаем значение числового элемента
	int getValue(std::string& s)
	{
		int n = s.size();
		int i = 1;
		while (i < n) if (!isDigit(s[i++])) break; //как только встретилась не цифра выходим
		return std::stoi(s.substr(0, i)); //преобразуем подстроку в целое число "245" в 245
	}

	//удаляем внешние скобки в строке
	std::string delbracket(std::string s)
	{
		// d будет 0 когда скобки скомпенсируются ()(()())
		for (int i = 1, k = 1; i < s.size(); i++, k += (s[i] == '(') - (s[i] == ')'))
			if (!k) return s.erase(0, 1).erase(i - 1, 1); //как только d==0 удаляем символы 0-й и (i-1)-й
		return s;
	}
};

class BinaryTree
{
	BinaryNode* root = nullptr;	//корень

	//печать дерева
	void Print(const std::string& s, const BinaryNode* n, bool k)
	{
		if (!n) return; //если элемента нет заканчиваем печать
		std::cout << s << (k ? "|__" : "|__") << n->value << "\n";
		Print(s + (k ? "|   " : "    "), n->left, 1);
		Print(s + (k ? "|   " : "    "), n->right, 0);
	}

public:
	BinaryTree(std::string s) : root(new BinaryNode(s)) {}

	//печать дерева
	void Print() { Print("", root, 0); };

	//нерекурсивный обход
	void NotRecursive()
	{
		std::vector<BinaryNode*> vector = { root };
		while (!vector.empty()) //пока не обойдем все дерево
		{
			BinaryNode* n = vector.back(); //обновление корня (рассматриваем потомков как корни со своими потомками)
			vector.pop_back(); //удаляем корень
			std::cout << n->value << " "; //выводим значение корня
			if (n->right) //если есть правый потомок добавляем его в стек 
				vector.push_back(n->right);
			if (n->left) //если есть левый потомок добавляем его в стек
				vector.push_back(n->left);
		}
		std::cout << "\n";
	}
};

int main()
{
	BinaryTree tree("8(3(1,6(4,7)),10(,14(13,)))");			//			 8			
	tree.Print();											//		3		  10	
	tree.NotRecursive();									//	 1	  6		     14	
}															//		 4  7		   13