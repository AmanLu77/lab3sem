﻿#include <iostream>
#include <stack>

bool Balanced(std::string& s)
{
    std::stack<char> stack; //стэк для накопления скобок
    for (char c : s)
    {
        switch (c)
        {
            //если открывающая скобка добавляем в стэк соответствующую закрывающую(!) скобку
        case '(':   stack.push(')'); break;
        case '{':   stack.push('}'); break;
        case '[':   stack.push(']'); break;
            //если закрывающая скобка , то проверяем стэк не пустой ли он и какая скобка лежит сверху
        case ')':   
        case '}':   
        case ']':   
            //если пустой — ошибка
            if (stack.empty()) {
                return false;
            }
            //если не совпадает верхняя скобка — ошибка
            if (stack.top() != c) {
                return false;
            }
            stack.pop(); //если ошибок нет удаляем последнюю скобку в стэке
            break;
        default:
            break;
        }
    }
    return stack.empty(); //если стек пустой то все правильно
}

int main()
{   
    std::string str;

    setlocale(LC_ALL, "Rus");
    std::cout << "Введите последовательность скобок: " << std::endl;
    std::cin >> str;
    /*
    std::string in1 = "({[]})";     // правильная строка
    std::string in2 = "[({[]})";    // лишняя открывающая
    std::string in3 = "({[)]})";    // несовпаледение откр и закр скобок
    */
    bool test = Balanced(str);
    if (test)
        std::cout << "Строка Правильная.\n";
    else
        std::cout << "Строка Неправильная.\n";

    return 0;
}
